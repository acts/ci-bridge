#!/usr/bin/env python3

import requests
import os
import json
import hmac

root_url = os.environ["CI_API_V4_URL"]
project_id = os.environ["CI_PROJECT_ID"]
pipeline_id = os.environ["CI_PIPELINE_ID"]
secret = os.environ["TRIGGER_SECRET"]
source_pr = json.loads(os.environ["SOURCE_PULL_REQUEST"])
report_url = os.environ["REPORT_URL"]

pipeline_info = json.loads(
    requests.get(f"{root_url}/projects/{project_id}/pipelines/{pipeline_id}").text
)

payload = {"source_pr": source_pr, "pipeline_info": pipeline_info}

# signature = hmac.new(secret.encode(), payload.encode(), digestmod="sha512").hexdigest()

# print(payload)
# print(signature)

# requests.post(
#     report_url,
#     data=payload,
#     headers={
#         "Content-Type": "application/json",
#         "X-CI-Bridge-Signature": signature,
#     },
# )

request = requests.Request(method="POST", url=report_url, json=payload, headers={})
prepped = request.prepare()
signature = hmac.new(secret.encode(), prepped.body, digestmod="sha512")
prepped.headers["X-CI-Bridge-Signature"] = signature.hexdigest()

with requests.Session() as session:
    response = session.send(prepped)
