#!/usr/bin/env python3

import os
import hmac


secret = os.environ["TRIGGER_SECRET"]
payload = os.environ["SOURCE_PULL_REQUEST"]
signature = os.environ["TRIGGER_SIGNATURE"]

expected = hmac.new(secret.encode(), payload.encode(), digestmod="sha512").hexdigest()

if not hmac.compare_digest(expected, signature):
    raise ValueError("Signatures do not match")
